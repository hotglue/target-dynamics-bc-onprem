from target_hotglue.client import HotglueSink
from requests_ntlm import HttpNtlmAuth
import requests
import logging
from datetime import datetime
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError


class DynamicsSink(HotglueSink):

    vendors = {}
    items = {}
    companies = {}
    tax_groups = {}

    @property
    def base_url(self) -> str:
        if self.config.get("base_url"):
            return self.config.get("base_url")

    @property
    def api_version(self) -> str:
        matches = ["beta", "v1.0"]
        if any([x in self.base_url for x in matches]):
            return "beta/v1"
        elif "v2.0" in self.base_url:
            return "v2"

    @property
    def http_headers(self):
        auth_credentials = {}
        return auth_credentials

    def validate_input(self, record: dict):
        return self.unified_schema(**record).dict()

    def _request(
        self, http_method, endpoint, params={}, request_data=None, headers={}
    ) -> requests.PreparedRequest:
        """Prepare a request object."""
        url = self.url(endpoint)
        headers.update(self.http_headers)
        params.update(self.params)
        auth = HttpNtlmAuth(self.config.get("username"), self.config.get("password"))
        logging.info(f"request endpoint {url} method {http_method} body {request_data}")
        response = requests.request(
            method=http_method,
            url=url,
            params=params,
            headers=headers,
            json=request_data,
            auth=auth,
        )
        logging.info(
            f"endpoint {url} response {response.text} status code {response.status_code}"
        )
        self.validate_response(response)
        return response

    def convert_date(self, date):
        if date:
            converted_date = date.split("T")[0]
            return converted_date
        return None

    def clean_convert(self, input):
        if isinstance(input, list):
            return [self.clean_convert(i) for i in input]
        elif isinstance(input, dict):
            output = {}
            for k, v in input.items():
                v = self.clean_convert(v)
                if isinstance(v, list):
                    output[k] = [i for i in v if (i)]
                elif v:
                    output[k] = v
            return output
        elif isinstance(input, datetime):
            return input.isoformat()
        elif input:
            return input

    def get_entity_ids(self, entity):

        if not self.companies:
            logging.info("Fetching companies")
            self.companies = self.get_companies()
            logging.info(f"COMPANIES: {self.companies}")
        offset = 0
        ids = {}

        for company in self.companies.values():
            url = f"/company({company})/{entity}"

            logging.info(f"Fetching {entity} for company {company}")
            while offset is not None:
                res = self.request_api("GET", url, params={"$skip": offset})
                if res.status_code == 404:
                    break
                if res.status_code != 200:
                    logging.info(f"REQUES STATUS CODE: {res.status_code}")
                    raise Exception(res.text)
                else:
                    logging.info(f"RESPONSE OF FETCHING ENTITY {entity} {res.text}")
                    results = res.json()["value"]
                    for result in results:
                        ids.update(
                            {
                                result["displayName"]: {
                                    "id": result["id"],
                                    "code": result.get("code", ""),
                                    "company_id": company,
                                },
                            }
                        )
                    logging.info(f"ids {ids}")
                    if results:
                        offset += len(results)
                    else:
                        offset = None
        logging.info(f"{entity} data: {ids}")
        return ids

    def get_companies(self):
        url = "/companies"
        res = self.request_api("GET", url)
        companies = {}
        if res.status_code != 200:
            logging.info(f"REQUES STATUS CODE: {res.status_code}")
            raise Exception(res.text)
        else:
            logging.info("Quering companies: \n")
            logging.info(res.text)

            for company in res.json()["value"]:
                companies[company["name"]] = company["id"]
            return companies

    def validate_response(self, response: requests.Response) -> None:
        """Validate HTTP response."""
        if response.status_code in [429] or 500 <= response.status_code < 600:
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)
        elif 400 <= response.status_code < 500 and response.status_code not in [404]:
            try:
                msg = response.text
            except:
                msg = self.response_error_message(response)
            raise FatalAPIError(msg)