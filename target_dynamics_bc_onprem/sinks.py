"""Dynamics-bc-onprem target sink class, which handles writing streams."""
from target_dynamics_bc_onprem.client import DynamicsSink
import logging


class VendorsSink(DynamicsSink):
    """Dynamics-bc-onprem target sink class."""

    endpoint = "/vendors"
    name = "Vendors"

    def preprocess_record(self, record: dict, context: dict) -> None:
        """Process the record."""

        if record.get("company_id"):
            self.endpoint = f"/companies({record.get('company_id')}){self.endpoint}"
        else:
            raise Exception(f"can't find company_id for the record {record}")
        
        payload = {
            "displayName": record.get("vendorName"),
            "email": record.get("emailAddress"),
            "currencyCode": record.get("currency"),
        }
        if record.get("addresses", []):
            address = record["addresses"][0]

            if self.api_version == "beta/v1":
                payload["address"] = {
                    "street": address.get("line1"),
                    "city": address.get("city"),
                    "state": address.get("state"),
                    "countryLetterCode": address.get("country"),
                    "postalCode": address.get("postalCode"),
                }

            elif self.api_version == "v2":
                payload["addressLine1"] = address.get("line1")
                payload["addressLine2"] = address.get("line2")
                payload["city"] = address.get("city")
                payload["state"] = address.get("state")
                payload["postalCode"] = address.get("postalCode")
                payload["country"] = address.get("country")

        if record.get("phoneNumbers", []):
            phoneNumber = record["phoneNumbers"][0].get("number")
            payload["phoneNumber"] = phoneNumber

        payload = self.clean_convert(payload)
        return payload

    def upsert_record(self, record: dict, context: dict):
        state_updates = dict()
        if record:
            self.logger.info(f"request to vendors with payload {record}")
            vendor = self.request_api(
                "POST", endpoint=self.endpoint, request_data=record
            )
            vendor_id = vendor.json()["id"]
            self.logger.info(f"BuyOrder created succesfully with Id {vendor_id}")
            return vendor_id, True, state_updates


class PurchaseOrdersSink(DynamicsSink):
    """Dynamics-bc-onprem target sink class."""

    endpoint = ""
    name = "PurchaseOrders"

    @property
    def endpoint_base(self) -> str:
        if self.api_version == "beta/v1":
            return "/purchaseOrders"
        elif self.api_version  == "v2":
            return "/purchaseInvoices"

    def preprocess_record(self, record: dict, context: dict) -> None:
        """Process the record."""

        # get vendors and items ids
        if not self.vendors:
            logging.info("Fetching vendors")
            self.vendors = self.get_entity_ids("vendors")
        if not self.items:
            logging.info("Fetching items")
            self.items = self.get_entity_ids("items")

        #get url
        if record.get("company_id"):
            if self.api_version == "beta/v1":
                self.endpoint = f"/companies({record.get('company_id')})/{self.endpoint_base}"
            elif self.api_version == "v2":
                self.endpoint = f"/companies({record.get('company_id')})/{self.endpoint_base}"
        else:
            raise Exception(f"can't find company_id for the record {record}")

        vendor_id = self.vendors.get(record.get("vendorName"), {}).get("id")

        if self.api_version == "beta/v1":
            purchase_order = {
                "vendorNumber": vendor_id,
            }
            lines = []
            for line in record.get("lineItems"):
                line_map = {
                    "itemId": self.items.get(line.get("name"), {}).get("id"),
                    "lineType": "Item",
                    "quantity": line.get("quantity")
                }
                lines.append(line_map)

        elif self.api_version == "v2":
            purchase_order = {
                "postingDate": self.convert_date(record.get("createdAt")),
                "dueDate": self.convert_date(record.get("dueDate")),
                "vendorId": vendor_id,
                "payToVendorId": vendor_id,
            }
            lines = []
            for line in record.get("lineItems"):
                line_map = {
                    "itemId": self.items.get(line.get("name"), {}).get("id"),
                    "lineType": "Item",
                    "description": line.get("description"),
                    "quantity": line.get("quantity"),
                    "directUnitCost": line.get("price"),
                    "taxCode": line.get("taxCode"),
                    "expectedReceiptDate": self.convert_date(line.get("serviceDate")),
                    "invoiceQuantity": line.get("quantity"),
                    "receiveQuantity": line.get("quantity"),
                }
                lines.append(line_map)

        purchase_order["invoiceDate"] = self.convert_date(record.get("createdAt")),
        purchase_order["currencyCode"] = record.get("currency")
        
        payload = {"purchase_order": purchase_order, "lines": lines}
        payload = self.clean_convert(payload)
        return payload

    def upsert_record(self, record: dict, context: dict):
        state_updates = dict()
        if record:
            self.logger.info(f"request to purchse orders with payload {record}")
            purchase_order = self.request_api(
                "POST",
                endpoint=self.endpoint,
                request_data=record.get("purchase_order"),
            )
            purchase_order = purchase_order.json()
            purchase_order_id = purchase_order.get("id")
            if purchase_order:
                for line in record.get("lines"):
                    purchase_order_lines = self.request_api(
                        "POST",
                        endpoint=f"/{self.endpoint}({purchase_order_id})/{self.endpoint_base[:-1]}Lines",
                        request_data=line,
                    )
            self.logger.info(
                f"purchase_order created succesfully with Id {purchase_order_id}"
            )
            return purchase_order_id, True, state_updates


class ItemsSink(DynamicsSink):
    """Dynamics-bc-onprem target sink class."""

    endpoint = "/items"
    name = "Items"

    def preprocess_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        if not self.tax_groups:
            logging.info("Fetching taxes")
            self.vendors = self.get_entity_ids("taxGroups")
        if record.get("company_id"):
            self.endpoint = f"/companies({record.get('company_id')}){self.endpoint}"
        else:
            raise Exception(f"can't find company_id for the record {record}")
        payload = {
            "number": record.get("sku"),
            "displayName": record.get("name"),
            "type": record.get("type"),
            "blocked": not (record.get("active", True)),
        }
        if self.tax_groups:
            if record.get("taxCode") in self.tax_groups.keys():
                payload["taxGroupCode"] = self.tax_groups[record.get("taxCode")]["code"]

        if self.api_version == "beta/v1":
            payload["itemCategory"] = {
                "categoryId": record.get("category"),
                "description": record.get("category"),
            }

        elif self.api_version == "v2":
            payload["itemCategoryCode"] = record.get("category")

        payload = self.clean_convert(payload)
        return payload

    def upsert_record(self, record: dict, context: dict):
        state_updates = dict()
        if record:
            self.logger.info(f"request to items with payload {record}")
            vendor = self.request_api(
                "POST", endpoint=self.endpoint, request_data=record
            )
            vendor_id = vendor.json().get("id")
            self.logger.info(f"BuyOrder created succesfully with Id {vendor_id}")
            return vendor_id, True, state_updates
