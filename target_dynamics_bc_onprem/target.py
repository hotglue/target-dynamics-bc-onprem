"""Dynamics-bc-onprem target class."""

from singer_sdk import typing as th

from target_dynamics_bc_onprem.sinks import VendorsSink, ItemsSink, PurchaseOrdersSink
from target_hotglue.target import TargetHotglue


class TargetDynamicsBcOnprem(TargetHotglue):
    """Sample target for Dynamics-bc-onprem."""

    SINK_TYPES = [VendorsSink, ItemsSink, PurchaseOrdersSink]
    MAX_PARALLELISM = 10
    name = "target-dynamics-bc-onprem"
    config_jsonschema = th.PropertiesList(
        th.Property("username", th.StringType, required=True),
        th.Property("password", th.StringType, required=True),
        th.Property(
            "url_base",
            th.StringType,
        ),
    ).to_dict()


if __name__ == "__main__":
    TargetDynamicsBcOnprem.cli()
